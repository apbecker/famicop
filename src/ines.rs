pub struct NesFile {
  pub chr: Vec<u8>,
  pub prg: Vec<u8>,
}

impl NesFile {
  pub fn load(path: &str) -> Option<Self> {
    let cart = std::fs::read(path).unwrap();

    let header = &cart[0..16];

    assert_eq!(&header[0..4], b"NES\x1a");
    assert_eq!(&header[6..16], &[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]);

    let prg_rom_size = (header[4] as usize) * 0x4000;
    let chr_rom_size = (header[5] as usize) * 0x2000;

    let prg = {
      let prg = &cart[16..];
      prg[..prg_rom_size].to_vec()
    };

    let chr = {
      let chr = &cart[16 + prg_rom_size..];
      chr[..chr_rom_size].to_vec()
    };

    Some(Self { chr, prg })
  }
}
