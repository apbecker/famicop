use crate::trace::TraceU8;

pub trait CpuAddressable {
  fn cpu_read_u8(&mut self, address: u16) -> u8;
  fn cpu_write_u8(&mut self, address: u16, val: TraceU8);

  fn cpu_read_u16(&mut self, address: u16) -> u16 {
    let lsbs = self.cpu_read_u8(address) as u16;
    let msbs = self.cpu_read_u8(address.wrapping_add(1)) as u16;

    (msbs << 8) | lsbs
  }
}

pub struct Cpu {
  pc: ProgramCursor,
  a: TraceU8,
  p: TraceU8,
  s: TraceU8,
  x: TraceU8,
  y: TraceU8,
}

impl Cpu {
  pub fn new(bus: &mut impl CpuAddressable) -> Self {
    Self {
      pc: ProgramCursor(bus.cpu_read_u16(0xfffc)),
      a: TraceU8::default(),
      p: TraceU8::default(),
      s: TraceU8::default(),
      x: TraceU8::default(),
      y: TraceU8::default(),
    }
  }

  pub fn absolute(&mut self, bus: &mut impl CpuAddressable) -> u16 {
    let lsbs = self.fetch(bus) as u16;
    let msbs = self.fetch(bus) as u16;

    (msbs << 8) | lsbs
  }

  pub fn zero_page(&mut self, bus: &mut impl CpuAddressable) -> u16 {
    let lsbs = self.fetch(bus) as u16;
    let msbs = 0 as u16;

    (msbs << 8) | lsbs
  }

  pub fn fetch(&mut self, cart: &mut impl CpuAddressable) -> u8 {
    cart.cpu_read_u8(self.pc.next())
  }

  pub fn bxx(&mut self, code: u8, offset: u8) -> Branch {
    let skip = self.pc.0;
    let take = self.pc.0.wrapping_add(offset as i8 as u16);
    let flag = match code >> 6 {
      0 => self.p.bit(N_FLAG),
      1 => self.p.bit(V_FLAG),
      2 => self.p.bit(C_FLAG),
      _ => self.p.bit(Z_FLAG),
    };

    let Some(flag) = flag else {
      return Branch::Both(skip, take);
    };

    let rhs = (code & (1 << 5)) != 0;

    if flag == rhs {
      Branch::Take(take)
    } else {
      Branch::Skip(skip)
    }
  }

  pub fn clc(&mut self) {
    self.p.set_bit(C_FLAG, false);
  }

  pub fn cld(&mut self) {
    self.p.set_bit(D_FLAG, false);
  }

  pub fn cli(&mut self) {
    self.p.set_bit(I_FLAG, false);
  }

  pub fn clv(&mut self) {
    self.p.set_bit(V_FLAG, false);
  }

  pub fn lda(&mut self, val: impl Into<TraceU8>) {
    self.a = val.into();
    self.nz(self.a);
  }

  pub fn ldx(&mut self, val: impl Into<TraceU8>) {
    self.x = val.into();
    self.nz(self.x);
  }

  pub fn ldy(&mut self, val: impl Into<TraceU8>) {
    self.y = val.into();
    self.nz(self.y);
  }

  pub fn sec(&mut self) {
    self.p.set_bit(C_FLAG, true);
  }

  pub fn sed(&mut self) {
    self.p.set_bit(D_FLAG, true);
  }

  pub fn sei(&mut self) {
    self.p.set_bit(I_FLAG, true);
  }

  pub fn sta(&self) -> TraceU8 {
    self.a
  }

  pub fn stx(&self) -> TraceU8 {
    self.x
  }

  pub fn sty(&self) -> TraceU8 {
    self.y
  }

  pub fn txa(&mut self) {
    self.a = self.x;
    self.nz(self.a);
  }

  pub fn tya(&mut self) {
    self.a = self.y;
    self.nz(self.a);
  }

  pub fn txs(&mut self) {
    self.s = self.x;
  }

  pub fn nz(&mut self, trace: TraceU8) {
    self.set_n_flag(trace);
    self.set_z_flag(trace);
  }

  fn set_n_flag(&mut self, trace: TraceU8) {
    if let Some(bit) = trace.bit(7) {
      self.p.set_bit(N_FLAG, bit);
    } else {
      self.p.set_bit(N_FLAG, None);
    }
  }

  fn set_z_flag(&mut self, trace: TraceU8) {
    // if the entire value is known, set `z` normally.

    if let Some(val) = trace.val() {
      self.p.set_bit(Z_FLAG, val == 0x00);
      return;
    }

    // if _any_ bit is known to be `1`, set `z` to false.

    for n in 0..8 {
      if let Some(true) = trace.bit(n) {
        self.p.set_bit(Z_FLAG, false);
        return;
      }
    }

    // can't determine `z`, unset it.

    self.p.set_bit(Z_FLAG, None);
  }
}

struct ProgramCursor(u16);

impl ProgramCursor {
  pub fn next(&mut self) -> u16 {
    let address = self.0;

    self.0 = self.0.wrapping_add(1);

    address
  }
}

const N_FLAG: u8 = 1 << 7;
const V_FLAG: u8 = 1 << 6;
const D_FLAG: u8 = 1 << 3;
const I_FLAG: u8 = 1 << 2;
const Z_FLAG: u8 = 1 << 1;
const C_FLAG: u8 = 1 << 0;

pub enum Branch {
  Both(u16, u16),
  Skip(u16),
  Take(u16),
}
