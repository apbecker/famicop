use famicop::cpu::{Branch, Cpu, CpuAddressable};
use famicop::ines::NesFile;
use famicop::nrom::NROM256;

fn main() {
  let ines = NesFile::load("/home/adam/Downloads/Micro Mages.nes").unwrap();
  let mut cart = NROM256::from_nes_file(ines);

  disassemble(&mut cart);
}

fn disassemble(cart: &mut impl CpuAddressable) {
  println!("nmi: 0x{:04x}", cart.cpu_read_u16(0xfffa));
  println!("res: 0x{:04x}", cart.cpu_read_u16(0xfffc));
  println!("irq: 0x{:04x}", cart.cpu_read_u16(0xfffe));

  let mut cpu = Cpu::new(cart);

  loop {
    let code = cpu.fetch(cart);

    if (code & 0x1f) == 0x10 {
      let offset = cpu.fetch(cart);

      match cpu.bxx(code, offset) {
        Branch::Both(_, _) => println!("branch is conditional"),
        Branch::Skip(_) => println!("branch is never taken"),
        Branch::Take(_) => println!("branch is always taken"),
      }

      continue;
    }

    match code {
      0x84 => {
        // STY $nn
        let address = cpu.zero_page(cart);
        let val = cpu.sty();
        cart.cpu_write_u8(address, val);
      }
      0x8d => {
        // STA $nnnn
        let address = cpu.absolute(cart);
        let val = cpu.sta();
        cart.cpu_write_u8(address, val);
      }
      0x8e => {
        // STX $nnnn
        let address = cpu.absolute(cart);
        let val = cpu.stx();
        cart.cpu_write_u8(address, val);
      }
      0x98 => cpu.tya(),
      0x9a => cpu.txs(),
      0xa0 => {
        // LDY #$nn
        let val = cpu.fetch(cart);
        cpu.ldy(val)
      }
      0xa2 => {
        // LDX #$nn
        let val = cpu.fetch(cart);
        cpu.ldx(val)
      }
      0xd8 => cpu.cld(),
      code => todo!("unimplemented: ${:02x}", code),
    }
  }
}
