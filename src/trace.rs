#[derive(Clone, Copy)]
pub struct TraceU8 {
  off: u8,
  on: u8,
}

impl Default for TraceU8 {
  fn default() -> Self {
    Self { off: 0, on: 0 }
  }
}

impl TraceU8 {
  pub fn from_u8(val: u8) -> Self {
    Self { off: !val, on: val }
  }

  pub fn bit(self, n: u8) -> Option<bool> {
    if (self.on & (1 << n)) != 0 {
      return Some(true);
    }

    if (self.off & (1 << n)) != 0 {
      return Some(false);
    }

    None
  }

  pub fn set_bit(&mut self, n: u8, val: impl Into<Option<bool>>) {
    let mask = 1 << n;

    match val.into() {
      None => {
        self.off &= !mask;
        self.on &= !mask;
      }
      Some(false) => {
        self.on &= !mask;
        self.off |= mask;
      }
      Some(true) => {
        self.on |= mask;
        self.off &= !mask;
      }
    }
  }

  pub fn val(self) -> Option<u8> {
    if self.known() == 255 {
      Some(self.on)
    } else {
      None
    }
  }

  /// Returns a value whose bits represent being `on` and `off` simultaneously.
  pub fn conflicts(self) -> u8 {
    self.on & self.off
  }

  /// Returns a value that represents which bits are in a `known` state.
  pub fn known(self) -> u8 {
    self.on | self.off
  }

  /// Returns a value that represents which bits are in an `unknown` state.
  pub fn unknown(self) -> u8 {
    !self.on & !self.off
  }
}

impl std::ops::BitAnd<u8> for TraceU8 {
  type Output = Self;

  fn bitand(self, rhs: u8) -> Self::Output {
    Self {
      off: self.off | !rhs,
      on: self.on & rhs,
    }
  }
}

impl std::ops::BitOr<u8> for TraceU8 {
  type Output = Self;

  fn bitor(self, rhs: u8) -> Self::Output {
    Self {
      off: self.off & !rhs,
      on: self.on | rhs,
    }
  }
}

impl std::ops::BitXor<u8> for TraceU8 {
  type Output = Self;

  fn bitxor(self, rhs: u8) -> Self::Output {
    Self {
      off: self.off ^ rhs,
      on: self.on ^ rhs,
    }
  }
}

impl From<u8> for TraceU8 {
  fn from(val: u8) -> Self {
    Self::from_u8(val)
  }
}

impl std::fmt::Display for TraceU8 {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    let known = self.known();

    for n in (0..8).rev() {
      if (known & (1 << n)) == 0 {
        write!(f, "?")?
      } else {
        if (self.on & (1 << n)) != 0 {
          write!(f, "1")?
        } else {
          write!(f, "0")?
        }
      }
    }

    Ok(())
  }
}

#[cfg(test)]
pub mod tests {
  use super::*;

  #[test]
  pub fn known_works() {
    let a = TraceU8::from_u8(0x55);
    assert_on_off(a, 0x55, 0xaa);
  }

  #[test]
  pub fn bitand_works() {
    let a = TraceU8::from_u8(0xc0);
    assert_on_off(a, 0xc0, 0x3f);

    let b = a & 0x40;
    assert_on_off(b, 0x40, 0xbf);
  }

  #[test]
  pub fn bitor_works() {
    let a = TraceU8::from_u8(0x80);
    assert_on_off(a, 0x80, 0x7f);

    let b = a | 0x40;
    assert_on_off(b, 0xc0, 0x3f);
  }

  #[test]
  pub fn bitxor_works() {
    let a = TraceU8::from_u8(0xf0);
    assert_on_off(a, 0xf0, 0x0f);

    let b = a ^ 0xaa;
    assert_on_off(b, 0x5a, 0xa5);
  }

  fn assert_on_off(trace: TraceU8, on: u8, off: u8) {
    assert_eq!(0x00, trace.conflicts());
    assert_eq!(0xff, trace.known());
    assert_eq!(0x00, trace.unknown());
    assert_eq!(on, trace.on);
    assert_eq!(off, trace.off);
  }
}
