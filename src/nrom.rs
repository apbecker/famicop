use crate::cpu::CpuAddressable;
use crate::ines::NesFile;
use crate::trace::TraceU8;

pub struct NROM256 {
  prg: Vec<u8>,
}

impl NROM256 {
  pub fn from_nes_file(file: NesFile) -> Self {
    Self { prg: file.prg }
  }
}

impl CpuAddressable for NROM256 {
  fn cpu_read_u8(&mut self, address: u16) -> u8 {
    self.prg[(address & 0x7fff) as usize]
  }

  fn cpu_write_u8(&mut self, address: u16, val: TraceU8) {
    if address > 0x7fff {
      println!("rom write, address={:04x}, val={}", address, val);
      return;
    }

    if address < 0x2000 {
      println!("ram write, address={:04x}, val={}", address, val);
      return;
    }

    match address {
      0x4015 => println!("apu status write, val={}", val),
      0x4017 => println!("apu frame ctrl write, val={}", val),
      _ => panic!("unhandled write, address={:04x}", address),
    }
  }
}
