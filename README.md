famicop
-------

An advanced disassembler and static analysis tool for Famicom/NES files.

# Gotchas

## Branch is always taken

Example from `Micro Mages`:

```
  ldy #$02 ; `z` is always `0` because the operand is always `#$02`
  bne +    ; always taken since `z` is always `0`
  ldy #$08 ; never executed
+:
  ...
```
